import os
import subprocess
from enum import Enum

class ChangedFile(object):
    def __init__(self, old_file, new_file, lines, disp_name = None):
        self.old = old_file
        self.new = new_file
        self.lines = lines
        if not disp_name:
            if self.new != '/dev/null':
                self.disp_name = self.new
            else:
                self.disp_name = self.old
        else:
            self.disp_name = disp_name

    def isAdded(self):
        return (self.old == '/dev/null')

    def isModified(self):
        return (self.old == self.new)

    def isDeleted(self):
        return (self.new == '/dev/null')

class LineType(Enum):
    UNTOUCHED = 1,
    REMOVED = 2,
    ADDED = 3

class ILine(object):
    def __init__(self, number, line_type, content):
        self.number = number
        self.type = line_type
        self.content = content

class FileLine(ILine):
    def __init__(self, full_line, number):
        if (len(full_line) > 1):
            content = full_line[1:]
            if full_line[0] == '-':
                line_type = LineType.REMOVED
            elif full_line[0] == '+':
                line_type = LineType.ADDED
            else:
                line_type = LineType.UNTOUCHED
        else:
            content = ''
            line_type = LineType.UNTOUCHED
        super().__init__(number, line_type, content)

class MessageLine(ILine):
    def __init__(self, line, number):
        super().__init__(number, LineType.ADDED, line)

class IAnalyzer(object):
    def getFiles(self):
        raise RuntimeError('not implemented yet!!!')

class ChangeAnalyzer(IAnalyzer):
    def __init__(self):
        self.files = []
        output = subprocess.run(['git', 'diff', '--color=never', '--ws-error-highlight=all', '--cached', '-U2000'],
                                stdout=subprocess.PIPE)\
                    .stdout.decode('utf-8')
        out_lines = output.splitlines()
        lines = []
        old_file = None
        new_file = None
        line_number = 1
        for line in out_lines:
            if line.startswith('diff --git a'):
                if old_file and new_file and lines and not self.__isExcludedFile(new_file):
                    self.files.append(ChangedFile(
                            old_file,
                            new_file,
                            lines
                        ))
                lines = []
                old_file = None
                new_file = None
                line_number = 1
            elif line.startswith('--- '):
                _, old_file = line.split(' ')
                if old_file != '/dev/null':
                    old_file = old_file[2:]
            elif line.startswith('+++ '):
                _, new_file = line.split(' ')
                if new_file != '/dev/null':
                    new_file = new_file[2:]
            elif line.startswith('@@'):
                _, number, line_part = line.split('@@', maxsplit=2)
                _, added = number.split('+')
                if ',' in added:
                    line_number_str,_ = added.split(',')
                else:
                    line_number_str,_ = added.split(' ')
                line_number = int(line_number_str)
                if line_part:
                    line_obj = FileLine(line_part, line_number - 1)
                    lines.append(line_obj)
            elif line[0] in [' ', '+', '-']:
                line_obj = FileLine(line, line_number)
                lines.append(line_obj)
                if line_obj.type != LineType.REMOVED:
                    line_number += 1
            # else information which is ignored by this parser
            # i.e.:
            #       new file mode 100644
            #       index 0000000..40423da

        if old_file and new_file and lines and not self.__isExcludedFile(new_file):
            self.files.append(ChangedFile(
                            old_file,
                            new_file,
                            lines
                        ))

    def __isExcludedFile(self, new_file):
        if new_file.endswith('.patch'):
            return True
        return False

    def getFiles(self):
        return self.files

class CommitAnalyzer(IAnalyzer):
    def __init__(self, commit_file):
        lines = []
        number = 1
        with open (commit_file, "r") as fileHandler:
            for line in fileHandler:
                lines.append(MessageLine(line.rstrip(os.linesep), number))
                number += 1
        self.file = ChangedFile('/dev/null', commit_file, lines, '<COMMIT MESSAGE>')

    def getFiles(self):
        return [ self.file ]

    def isRevertCommit(self):
        lines = self.file.lines
        if len(lines)>0:
            return lines[0].content.startswith('Revert ')
        return False