import os
import subprocess

class HookScanner(object):
    def __init__(self, commit_file):
        self.commit_file = commit_file
        self.hooks_dir = os.path.join(os.path.dirname(commit_file), 'hooks')
        if not os.path.exists(self.hooks_dir) or \
           not os.path.isdir(self.hooks_dir):
           self.hooks_dir = None

    def __exec_script(self, script):
        process = subprocess.run([script, self.commit_file], stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)

        print(process.stdout.decode('utf-8'))
        return process.returncode

    def execCommitMsgHook(self):
        if not self.hooks_dir: return 0

        commit_msg_script = os.path.join(self.hooks_dir, 'commit-msg')
        if not os.path.exists(commit_msg_script): return 0

        return self.__exec_script(commit_msg_script)