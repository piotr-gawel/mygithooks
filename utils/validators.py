import subprocess
from utils.changes import LineType

class IValidator(object):
    def validate(self, changed_file):
        raise RuntimeError('not implemented yet!!!')

    @staticmethod
    def printError(file_name, line_number, message):
        print("ERROR: {0}:{1}: {2}".format(file_name, line_number, message))

class IndentationValidator(IValidator):
    def validate(self, changed_file):
        indent = self.__getDominatingIndent(changed_file.lines)
        if indent is None:
            # No indentation even in new lines. e
            return True

        return self.__checkNewLines(changed_file, changed_file.lines, indent)

    @staticmethod
    def __getDominatingIndent(lines):
        index = 1
        added_indent = None
        for line in lines:
            if line.type == LineType.UNTOUCHED:
                content = line.content
                if len(content) > 0:
                    if content[0] in [' ', '\t']:
                        return content[0]
            elif not added_indent and line.type == LineType.ADDED:
                content = line.content
                if len(content) > 0:
                    if content[0] in [' ', '\t']:
                        added_indent = content[0]
        return added_indent

    @staticmethod
    def __checkNewLines(file, lines, indent):
        result = True
        for line in lines:
            content = line.content
            line_len = len(content)
            if line_len > 0 and line.type == LineType.ADDED:
                index  = 0
                error = ''
                space_count = 0
                while index < line_len:
                    if content[index] in [' ', '\t']:
                        if content[index] != indent:
                            if content[index] == ' ' and index > 0:
                                space_count += 1
                                if space_count > 3:
                                    IValidator.printError(file.disp_name, line.number,
                                                      'indentation mismatch in line `{0}`'.format(content.strip()))
                                    result = False
                                    break
                            else:
                                IValidator.printError(file.disp_name, line.number,
                                                  'incorrect indentation in line `{0}`'.format(content.strip()))
                                result = False
                                break
                        elif content[index] == '\t' and space_count > 0:
                            IValidator.printError(file.disp_name, line.number,
                                              'mix of tabulators and spaces at the beginning of line `{0}`'.format(content.strip()))
                            result = False
                            break
                    else:
                        break
                    index += 1
                if index == line_len:
                    IValidator.printError(file.disp_name, line.number, 'line contains only tabulators and/or spaces')
                    result = False
        return result

class LineEndValidator(IValidator):
    def validate(self, changed_file):
        result = True
        for line in changed_file.lines:
            if line.type == LineType.ADDED:
                content = line.content
                if content.endswith(' ') or content.endswith('\t'):
                    IValidator.printError(changed_file.disp_name, line.number,
                                          'line `{0}` ends with space and/or tabulator'.format(content.strip()))
                    result = False

        return result

class CommitSubjectValidator(IValidator):
    def __printError(self, file_name, message):
        EXPECTED_FORMAT = "\n\tEXPECTED arch|subsystem: Description.\n"
        IValidator.printError(file_name, 1, "{0}{1}".format(message, EXPECTED_FORMAT))

    def validate(self, changed_file):
        lines = changed_file.lines
        if len(lines) < 2:
            IValidator.printError(changed_file.disp_name, 1,
                                  'no commit message or subject without next empty line')
            return False
        result = True
        subject = lines[0].content

        if ':' not in subject:
            self.__printError(changed_file.disp_name, 'incorrect format in the subject (missing \':\')')
            result = False
        if not subject.endswith('.'):
            self.__printError(changed_file.disp_name, 'missing \'.\' at the end of subject')
            result = False
        if subject.startswith(' ') or subject.endswith('\t'):
            self.__printError(changed_file.disp_name, 'subject must not start with whitespace character')
            result = False
        sections = subject.split(':')
        for section in sections[1:]:
            if not section.startswith(' '):
                self.__printError(changed_file.disp_name, 'missing <space> after \':\' in subject')
                result = False
        for section in sections:
            if not section.strip():
                self.__printError(changed_file.disp_name, 'multiple \':\' without text between in subject')
                result = False
                break
            elif section.endswith(' ') or section.endswith('\t'):
                self.__printError(changed_file.disp_name, 'extra whitespace characters at the end of each fragment (arch or description) in subject')
                result = False
                break

        for section in sections[0:-1]:
            stripped_section = section.strip()
            if ' ' in stripped_section or '\t' in stripped_section:
                self.__printError(changed_file.disp_name, 'whitespace characters are not allowed in arch|subsystem section')
                result = False

        stripped_description = sections[-1].strip()
        if not stripped_description or not stripped_description[0].isupper():
            self.__printError(changed_file.disp_name, 'last part of the subject must start with upper letter')
            result = False

        if lines[1].content.strip():
            IValidator.printError(changed_file.disp_name, 2,
                                  'second line (one after subject) must be empty')
            result = False
        return result

class CommitIssueValidator(IValidator):
    def validate(self, changed_file):
        for line in changed_file.lines:
            if line.content.startswith('Issue: '):
                if line.number < 3:
                    IValidator.printError(changed_file.disp_name, 0,
                                          'issue number must not be located in lines dedicated for subject')
                return True
        IValidator.printError(changed_file.disp_name, 0,
                              'missing issue number in commit message')
        return False

class AspellValidator(IValidator):
    def validate(self, changed_file):
        output = subprocess.getoutput('aspell --mode=email --add-email-quote=\'#\' --lang=en list < {0} | sort | uniq'.format(
                                            changed_file.new))
        print("\n        ========    ASPELL RESULTS    ========")
        print(output)
        print("        ======================================\n")
        return True