# Installation

1. Copy hooks to some directory, i.e. $HOME/git-hooks
2. Set GIT global configuration to look for hooks in selected directory:

```shell
git config --global core.hooksPath $HOME/git-hooks
```

# Running GIT without hooks

When committing, add --no-verify option:
```shell
git commit --no-verify
```